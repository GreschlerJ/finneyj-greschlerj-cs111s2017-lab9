/* ************************************************************************
The work we are submitting is a result of our own thinking and efforts
Jahlia Finney and Josh Greschler
CMPSC 111 Spring 2017
Lab 9
Date: 4/5/17
*********************************************************************/

//imports classes to be used
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class TodoList {

	//declares the array todoItems to be type TodoItem
	private ArrayList<TodoItem> todoItems;
	private static final String TODOFILE = "todo.txt";

	public TodoList() {
		todoItems = new ArrayList<TodoItem>();
	}

	public void addTodoItem(TodoItem todoItem) {
		todoItems.add(todoItem);
	}

	public Iterator getTodoItems() {
		return todoItems.iterator();
	}
	//declares method readTodoItemsFromFile which was called in the main method
	public void readTodoItemsFromFile() throws IOException {
		//scans the lines of the .txt file
		Scanner fileScanner = new Scanner(new File(TODOFILE));
		while(fileScanner.hasNext()) {
			String todoItemLine = fileScanner.nextLine();
			Scanner todoScanner = new Scanner(todoItemLine);
			
			//parces up the item into 3 parts and adds them to each element in the array todoItem
			todoScanner.useDelimiter(",");
			String priority, category, task;
			priority = todoScanner.next();
			category = todoScanner.next();
			task = todoScanner.next();
			TodoItem todoItem = new TodoItem(priority, category, task);
			todoItems.add(todoItem);
		}
	}

	//declares the markTaskAsDone method in the TodoList class
	public void markTaskAsDone(int toMarkId) {
		//finds the element in todoItems marked with the input ID and marks it as done
		Iterator iterator = todoItems.iterator();
		while(iterator.hasNext()) {
			TodoItem todoItem = (TodoItem)iterator.next();
			if(todoItem.getId() == toMarkId) {
				todoItem.markDone();
			}
		}
	}

	//declares the findTasksOfPriority method
	public Iterator findTasksOfPriority(String requestedPriority) {
		//declares the array priorityList to be of type TodoItem
		ArrayList<TodoItem> priorityList = new ArrayList<TodoItem>(); //the ArrayList for todoitem
		Iterator iterator = todoItems.iterator(); //
		
		//scans the elements of todoItems and compares the priority to the requested priority
		while(iterator.hasNext()) { 
			TodoItem todoItem =  (TodoItem)iterator.next(); 
			if(todoItem.getPriority().equals(requestedPriority)) { //
			//if priorities match, adds the element to the array priorityList
			priorityList.add(todoItem);
                     } 
		}     return priorityList.iterator();
	} 

	//declares findTasksofCategory method which does the same thing as the priority method but looks at the Category part of each element and saves it to the categoryList array
	public Iterator findTasksOfCategory(String requestedCategory) {
		ArrayList<TodoItem> categoryList = new ArrayList<TodoItem>();
		Iterator iterator = todoItems.iterator(); 
		while(iterator.hasNext()) {
			TodoItem todoItem = (TodoItem)iterator.next(); 
			if(todoItem.getCategory().equals(requestedCategory)) {   
				categoryList.add(todoItem);
			}
		} return categoryList.iterator();
	} 

	//declares toString method which changes an object of type TodoItem to type String in order to be printed
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		Iterator iterator = todoItems.iterator();
		while(iterator.hasNext()) {
			buffer.append(iterator.next().toString());
			if(iterator.hasNext()) {
				buffer.append("\n");
			}
		}
		return buffer.toString();
	}

}

