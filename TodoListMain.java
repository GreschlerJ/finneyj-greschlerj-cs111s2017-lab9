/* ************************************************************
The work we are submitting is a result of our own thinking and efforts
Jahlia Finney and Josh Greschler
CMPSC 111 Spring 2017
Lab 9
Date: 4/5/17

Pourpse: The pourpse of this program is to serve as a Todo list manager, offering a variety of options to search for and manage items
**************************************************************/
import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

//declares main method
public class TodoListMain {

    public static void main(String[] args) throws IOException {
	
	//prints header asking for user to input command
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, done, list, quit");

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();

        //repeats as long as the command is not 'quit'
	while(scanner.hasNext()) {
            
	    //reads command from user
	    String command = scanner.nextLine();
            
 	    //sorts command into different tasks
	    
	    //if the user inputs read, the program calls the readTodoItemsFromFile method in the todoList class
  	    if(command.equals("read")) {
                todoList.readTodoItemsFromFile();
            }
	    //if the user inputs list, the program prints the array todoList as a series of strings
            else if(command.equals("list")) {
                System.out.println(todoList.toString());
            }
            
	    //if the user inputs priority-search, the program asks the user for a desired priority to search for. it then passes that value to the method findTasksOfPriority and prints the returned array.
	    else if(command.equals("priority-search")) {
                System.out.println("What is the priority?");
                String requestedPriority = scanner.nextLine();
                Iterator priorityIterator = todoList.findTasksOfPriority(requestedPriority);
               //allows the print statement to repeat for every element 
	       while(priorityIterator.hasNext()) {
                    System.out.println((TodoItem)priorityIterator.next());
                }
            }
            
	     //category search does the same thing as priority search, but calls a different method
	     else if(command.equals("category-search")) {
                System.out.println("What is the category?");
                String requestedCategory = scanner.nextLine();
                Iterator categoryIterator = todoList.findTasksOfCategory(requestedCategory);
                while(categoryIterator.hasNext()) {
                    System.out.println((TodoItem)categoryIterator.next());
                }
            }
           //done allows the user to mark an element in the list as done 
	   else if(command.equals("done")) {
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsDone(chosenId);
            }
           //quit breaks the loop and finishes the program 
	   else if(command.equals("quit")) {
                break;
            }
        }

    }

}
